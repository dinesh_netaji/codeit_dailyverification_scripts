import com.kms.katalon.core.logging.KeywordLogger
import com.kms.katalon.core.exception.StepFailedException
import com.kms.katalon.core.reporting.ReportUtil
import com.kms.katalon.core.main.TestCaseMain
import com.kms.katalon.core.testdata.TestDataColumn
import groovy.lang.MissingPropertyException
import com.kms.katalon.core.testcase.TestCaseBinding
import com.kms.katalon.core.driver.internal.DriverCleanerCollector
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.configuration.RunConfiguration
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import internal.GlobalVariable as GlobalVariable

Map<String, String> suiteProperties = new HashMap<String, String>();


suiteProperties.put('id', 'Test Suites/Model/BHH')

suiteProperties.put('name', 'BHH')

suiteProperties.put('description', '')
 

DriverCleanerCollector.getInstance().addDriverCleaner(new com.kms.katalon.core.webui.contribution.WebUiDriverCleaner())
DriverCleanerCollector.getInstance().addDriverCleaner(new com.kms.katalon.core.mobile.contribution.MobileDriverCleaner())
DriverCleanerCollector.getInstance().addDriverCleaner(new com.kms.katalon.core.cucumber.keyword.internal.CucumberDriverCleaner())



RunConfiguration.setExecutionSettingFile("C:\\Users\\qatester4\\Desktop\\Dailyprod_DD\\DailyProdDataDriven\\Reports\\Model\\BHH\\20190401_121240\\execution.properties")

TestCaseMain.beforeStart()

TestCaseMain.startTestSuite('Test Suites/Model/BHH', suiteProperties, [new TestCaseBinding('Test Cases/Login', 'Test Cases/Login',  [ 'userID' : 'prabhaharan.velu@gsihealth.com' , 'url' : 'https://healthhome.gsihealth.com' , 'ENV' : 'BHH' , 'password' : 'Imax123#' ,  ]), new TestCaseBinding('Test Cases/TaskManagerOverDueCount', 'Test Cases/TaskManagerOverDueCount',  null), new TestCaseBinding('Test Cases/TaskManagerNavigation', 'Test Cases/TaskManagerNavigation',  null), new TestCaseBinding('Test Cases/AddPatientToPatientPanel', 'Test Cases/AddPatientToPatientPanel',  [ 'patientID' : '4238' , 'patientFirstName' : 'Zeppelin' , 'patientLastName' : 'Bram' ,  ]), new TestCaseBinding('Test Cases/DemographicsAddressFieldUpdate', 'Test Cases/DemographicsAddressFieldUpdate',  [ 'patientID' : '4238' , 'patientLastName' : 'Bram' ,  ]), new TestCaseBinding('Test Cases/CareTeamAssignandRemove', 'Test Cases/CareTeamAssignandRemove',  [ 'ExistingCareTeamNameSelect' : 'GSITESTCT A' , 'patientID' : '4238' , 'patientLastName' : 'Bram' ,  ]), new TestCaseBinding('Test Cases/AddNewProgram', 'Test Cases/AddNewProgram',  [ 'patientID' : '4238' , 'patientLastName' : 'Bram' ,  ]), new TestCaseBinding('Test Cases/CarePlanEncounterUpdate', 'Test Cases/CarePlanEncounterUpdate',  [ 'patientID' : '4238' , 'patientLastName' : 'Bram' ,  ]), new TestCaseBinding('Test Cases/CarePlanCCPUpdate', 'Test Cases/CarePlanCCPUpdate',  [ 'objectiveGoal' : 'test' , 'interventionStatus' : 'Deferred' , 'interventionPlan' : 'test' , 'patientID' : '4238' , 'goalStatus' : 'In-Progress' , 'patientLastName' : 'Bram' , 'goalType' : 'Long Term' , 'issuesTitle' : 'Healthhome' , 'goalStartDate' : '12-Sep-2018' ,  ]), new TestCaseBinding('Test Cases/MessageMailVerification', 'Test Cases/MessageMailVerification',  [ 'ToMailID' : 'prabhaharan.velu_gsihealth@swbdirect.gsihealth.net' ,  ]), new TestCaseBinding('Test Cases/SOPReport', 'Test Cases/SOPReport',  [ 'parentProgram' : 'Brooklyn Health Home Special Populations' , 'primaryPayerClass' : 'ALL' , 'programStatus' : 'ALL' , 'secondaryPayerClass' : 'ALL' , 'programName' : 'ALL' , 'organization' : 'ALL' ,  ]), new TestCaseBinding('Test Cases/InterActiveReport', 'Test Cases/InterActiveReport',  null), new TestCaseBinding('Test Cases/CareBookNavigation', 'Test Cases/CareBookNavigation',  [ 'patientID' : '4238' , 'patientFirstName' : 'Zeppelin' , 'patientLastName' : 'Bram' ,  ]), new TestCaseBinding('Test Cases/UpdateUser', 'Test Cases/UpdateUser',  [ 'userID' : 'prabhaharan.velu@gsihealth.com' , 'userFirstName' : 'Prabhaharan' , 'userLastName' : 'Velu' ,  ]), new TestCaseBinding('Test Cases/AlertVerification', 'Test Cases/AlertVerification',  [ 'patientID' : '4238' ,  ])])
