<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>VerifyPatientPresent</name>
   <tag></tag>
   <elementGuidId>6172e356-21c0-4fad-b704-17bbf82f8616</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//div[@class=&quot;q-item-side q-item-section flex q-item-side-left&quot;] | //div[@class=&quot;md-list-item-text fade flex&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//div[@class=&quot;q-item-side q-item-section flex q-item-side-left&quot;] | //div[@class=&quot;md-list-item-text fade flex&quot;]</value>
   </webElementProperties>
</WebElementEntity>
