<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>BTQ Encounter Activity Verification</name>
   <tag></tag>
   <elementGuidId>d5f5268d-18c9-4868-a09d-4bc5ceb174af</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value> //td[text()='Patient ID']/following::tr[1]/td[5]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value> //td[text()='Patient ID']/following::tr[1]/td[5]</value>
   </webElementProperties>
</WebElementEntity>
