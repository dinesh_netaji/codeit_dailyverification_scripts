<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>PR_InsightsActions</name>
   <tag></tag>
   <elementGuidId>d18bc822-6680-4257-876f-ca80d4e69ced</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//div[contains(text(),'Insights &amp; Actions')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//div[contains(text(),'Insights &amp; Actions')]</value>
   </webElementProperties>
</WebElementEntity>
