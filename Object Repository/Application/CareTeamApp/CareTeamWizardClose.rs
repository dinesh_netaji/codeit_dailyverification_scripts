<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>CareTeamWizardClose</name>
   <tag></tag>
   <elementGuidId>64d8c55b-4229-42f6-a418-0543a4852131</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//button[@class=&quot;md-icon-button md-button md-ink-ripple flex-nogrow&quot;]</value>
   </webElementProperties>
</WebElementEntity>
