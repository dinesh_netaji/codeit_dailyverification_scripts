<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>CTA_Bottom_SearchButton</name>
   <tag></tag>
   <elementGuidId>a51f3d29-aabb-4e58-b2fb-17fe1eb32287</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>(//div[@id='tabappCareteams']//div[normalize-space(text()) ='Search'])[2]</value>
   </webElementProperties>
</WebElementEntity>
