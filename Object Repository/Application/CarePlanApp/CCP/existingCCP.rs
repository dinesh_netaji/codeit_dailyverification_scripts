<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>existingCCP</name>
   <tag></tag>
   <elementGuidId>0f5af9af-9ad5-4b4d-97c7-8a05eeb31b4d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//a[@class='issueLink'] | //a[contains(@id,'issueLink')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//a[@class='issueLink'] | //a[contains(@id,'issueLink')]</value>
   </webElementProperties>
</WebElementEntity>
