<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>samshaPopupConfirmation</name>
   <tag></tag>
   <elementGuidId>6d98a69e-e745-471e-89a7-c795f84b7cb3</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//label[contains(text(),'I have read and ')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//label[contains(text(),'I have read and ')]</value>
   </webElementProperties>
</WebElementEntity>
